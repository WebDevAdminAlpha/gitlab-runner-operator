#!/bin/bash

set -eo pipefail

source scripts/_common.sh

echo "Creating release config at hack/assets/release.yaml"

if "$CERTIFIED"; then
  cat > hack/assets/release.yaml << EOF
  version: $OPERATOR_VERSION
  images:
    - name: gitlab-runner
      upstream: $UPSTREAM_UBI_IMAGES_REPOSITORY/gitlab-runner-ocp:$RUNNER_REVISION
      certified: registry.connect.redhat.com/gitlab/gitlab-runner$(image_digest "runner")
    - name: gitlab-runner-helper
      upstream: $UPSTREAM_UBI_IMAGES_REPOSITORY/gitlab-runner-helper-ocp:x86_64-$RUNNER_REVISION
      certified: registry.connect.redhat.com/gitlab/gitlab-runner-helper$(image_digest "helper")
EOF
else
  cat > hack/assets/release.yaml << EOF
  version: $OPERATOR_VERSION
  images:
    - name: gitlab-runner
      upstream: $UPSTREAM_UBI_IMAGES_REPOSITORY/gitlab-runner-ocp:$RUNNER_REVISION
    - name: gitlab-runner-helper
      upstream: $UPSTREAM_UBI_IMAGES_REPOSITORY/gitlab-runner-helper-ocp:x86_64-$RUNNER_REVISION
EOF
fi