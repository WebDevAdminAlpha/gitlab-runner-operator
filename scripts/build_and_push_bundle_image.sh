#!/bin/bash

set -eo pipefail

source scripts/_common.sh

if "$CERTIFIED"; then
  OPERATOR_PROJECT="$REDHAT_OPERATOR_IMAGES_REPOSITORY"
  OPERATOR_BUNDLE_PROJECT="$GITLAB_RUNNER_OPERATOR_BUNDLE_CONNECT_NAMESPACE"
else
  OPERATOR_PROJECT="$GITLAB_RUNNER_OPERATOR_UPSTREAM_NAMESPACE"
  OPERATOR_BUNDLE_PROJECT="$GITLAB_RUNNER_OPERATOR_BUNDLE_UPSTREAM_NAMESPACE"
fi

echo "Building bundle for operator image $PROJECT/$GITLAB_RUNNER_OPERATOR_BUNDLE_IMAGE"

make IMG="$OPERATOR_PROJECT/gitlab-runner-operator$(image_digest "operator")" bundle
source ./scripts/create_related_images_config.sh
source ./scripts/set_csv_versions.sh

# REVISION is already included in the $OPERATOR_VERSION that was passed down from the initial make target
make PROJECT="$OPERATOR_BUNDLE_PROJECT" VERSION="$OPERATOR_VERSION" REVISION="" \
 certification-bundle certification-bundle-build certification-bundle-tag
login_and_push bundle "$CERTIFIED"