#!/bin/bash

set -eo pipefail

source scripts/_common.sh

echo "Tagging and pushing UBI images"

RUNNER_UBI_IMAGE="$UPSTREAM_UBI_IMAGES_REPOSITORY/gitlab-runner-ocp:$RUNNER_REVISION"
docker pull "$RUNNER_UBI_IMAGE"
docker tag "$RUNNER_UBI_IMAGE" "$GITLAB_RUNNER_IMAGE"
login_and_push runner "$CERTIFIED"

RUNNER_HELPER_UBI_IMAGE="$UPSTREAM_UBI_IMAGES_REPOSITORY/gitlab-runner-helper-ocp:x86_64-$RUNNER_REVISION"
docker pull "$RUNNER_HELPER_UBI_IMAGE"
docker tag "$RUNNER_HELPER_UBI_IMAGE" "$GITLAB_RUNNER_HELPER_IMAGE"
login_and_push helper "$CERTIFIED"
